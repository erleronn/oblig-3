package no.ntnu.idatt2001.cardgame.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import no.ntnu.idatt2001.cardgame.core.DeckOfCards;
import no.ntnu.idatt2001.cardgame.core.HandOfCards;
import no.ntnu.idatt2001.cardgame.core.PlayingCard;


public class Controller {
    @FXML
    ImageView card1 = new ImageView();
    @FXML
    ImageView card2 = new ImageView();
    @FXML
    ImageView card3 = new ImageView();
    @FXML
    ImageView card4 = new ImageView();
    @FXML
    ImageView card5 = new ImageView();
    @FXML
    TextField flush = new TextField();
    @FXML
    TextField hearts = new TextField();
    @FXML
    TextField queenOfSpades = new TextField();
    @FXML
    TextField sumOfFaces = new TextField();


    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand;

    Image image1;
    Image image2;
    Image image3;
    Image image4;
    Image image5;

    public void dealHand(ActionEvent e){
        reset();
        hand = deck.dealHand(5);
        System.out.println(hand.toString());
        try{
            image1 = new Image((this.getClass().getResource(generateImagePath(hand.getCards().get(0)))).toExternalForm());
            image2 = new Image((this.getClass().getResource(generateImagePath(hand.getCards().get(1)))).toExternalForm());
            image3 = new Image((this.getClass().getResource(generateImagePath(hand.getCards().get(2)))).toExternalForm());
            image4 = new Image((this.getClass().getResource(generateImagePath(hand.getCards().get(3)))).toExternalForm());
            image5 = new Image((this.getClass().getResource(generateImagePath(hand.getCards().get(4)))).toExternalForm());
            card1.setImage(image1);
            card2.setImage(image2);
            card3.setImage(image3);
            card4.setImage(image4);
            card5.setImage(image5);
        } catch (Exception error){
            System.out.println(error.getMessage());
        }


    }
    public void checkHand(ActionEvent e){
        hearts.setText(hand.getCardsOfHeart());
        if(hand.isFlush()){
            flush.setText("Yes");
        } else{
            flush.setText("No");
        }
        if(hand.containsS12()){
            queenOfSpades.setText("Yes");
        } else{
            queenOfSpades.setText("No");
        }
        sumOfFaces.setText(String.valueOf(hand.getSum()));
    }
    private void reset(){
        image1 = null;
        image2 = null;
        image3 = null;
        image4 = null;
        image5 = null;
        flush.setText("");
        queenOfSpades.setText("");
        sumOfFaces.setText("");
        hearts.setText("");
    }

    private String generateImagePath(PlayingCard card){
        return ("/no.idatt2001.cardgame/images/PNG/" + card.getFace() + card.getSuit() + ".png");
    }
}
