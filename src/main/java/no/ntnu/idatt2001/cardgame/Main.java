package no.ntnu.idatt2001.cardgame;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;


public class Main extends Application{

    public static void main (String[] args){
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        try {
            Parent root = FXMLLoader.load(this.getClass().getResource("/no.idatt2001.cardgame/Main.fxml"));
            Scene scene = new Scene(root);
            stage.setScene(scene);

            Image icon = new Image((this.getClass().getResource("/no.idatt2001.cardgame/card_icon.png")).toExternalForm());
            stage.getIcons().add(icon);
            stage.setTitle("Card game");
            stage.setResizable(false);

            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

