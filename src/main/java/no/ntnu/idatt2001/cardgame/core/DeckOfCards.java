package no.ntnu.idatt2001.cardgame.core;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class DeckOfCards {
    private final List<PlayingCard> cardList;

    /**
     * Constructor creating a deck of cards as an arraylist.
     */
    public DeckOfCards(){
        cardList = new ArrayList<>();
        char[] suits = {'H', 'D', 'C', 'S'};
        for (char s : suits){
            for (int i = 1; i <= 13; i++){
                PlayingCard card = new PlayingCard(s,i);
                cardList.add(card);
            }
        }
    }

    /**
     * Method that deals a hand of n playing cards. This method takes random card from a copy of the card list,
     * generated every time the method is called.
     * @param n
     * @return
     * @throws IllegalArgumentException
     */
    public HandOfCards dealHand(int n) throws IllegalArgumentException{
        if(n <= 0){
            throw new IllegalArgumentException();
        }

        ArrayList<PlayingCard> cardListCopy = new ArrayList<>(cardList);
        ArrayList<PlayingCard> hand = new ArrayList<>();

        for(int k = 0; k < n; k++){
            int randomNum = getRandom(1,cardListCopy.size()-1);
            PlayingCard added = cardListCopy.get(randomNum);

            hand.add(added);
            cardListCopy.remove(added);
        }

        return new HandOfCards(hand);
    }

    private ArrayList<Integer> initNumList(int size){
        ArrayList<Integer> numList = new ArrayList<>();
        IntStream stream = IntStream.range(0,size);
        stream.forEach(numList::add);
        return numList;
    }
    public static int getRandom(int from, int to) {
        if (from < to)
            return from + new Random().nextInt(Math.abs(to - from));
        return from - new Random().nextInt(Math.abs(to - from));
    }
}
