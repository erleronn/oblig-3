package no.ntnu.idatt2001.cardgame.core;

import java.util.ArrayList;

public class HandOfCards {
    private final ArrayList<PlayingCard> cards;
    public HandOfCards(ArrayList<PlayingCard> cards){
                if(cards.isEmpty()){
            throw new IllegalArgumentException();
        }
        this.cards = cards;
    }

    public int getSum(){
        int sum;
        sum = cards.stream().mapToInt(PlayingCard::getFace).sum();
        return sum;
    }

    public String getCardsOfHeart(){
        ArrayList<String> heartCards = new ArrayList<>();
        cards.stream().filter(s -> s.getAsString().startsWith("H"))
                .forEach(heart -> heartCards.add(heart.getAsString()));
        StringBuilder hearts = new StringBuilder();
        for(String s: heartCards){
            hearts.append(s).append(" ");
        }
        if(heartCards.size() == 0){
            hearts.append("None");
        }
        return hearts.toString();
    }

    public boolean containsS12(){
        return cards.stream().anyMatch(s -> s.getAsString().equals("S12"));
    }

    public boolean isFlush(){
       int numHearts = (int) cards.stream().filter(s -> s.getSuit() == 'H').count();
       int numDiamonds = (int) cards.stream().filter(s -> s.getSuit() == 'D').count();
       int numClubs = (int) cards.stream().filter(s -> s.getSuit() == 'C').count();
       int numSpades = (int) cards.stream().filter(s -> s.getSuit() == 'S').count();

    return numHearts >= 5 || numDiamonds >= 5 || numClubs >= 5 || numSpades >= 5;
    }

    public ArrayList<PlayingCard> getCards(){
        return cards;
    }

    @Override
    public String toString(){
        StringBuilder display = new StringBuilder();
        display.append("|");
        for(PlayingCard card : cards){
            display.append(card.getAsString()).append("|");
        }
        return display.toString();
    }
}
