module no.ntnu.idatt2001.cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    exports no.ntnu.idatt2001.cardgame;
    opens no.ntnu.idatt2001.cardgame.gui to javafx.fxml;
}